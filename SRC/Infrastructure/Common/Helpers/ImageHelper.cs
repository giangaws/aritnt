﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Common.Helpers
{
	public class ImageHelper
	{
		public static string SaveImage(string path, string prefix, string id, string type, string data)
		{
			string imageLink = string.Empty;

			string pathImage = $"{path}{prefix}";
			CommonHelper.CheckAndCreateDirectory(pathImage);
			File.WriteAllBytes($"{pathImage}/{id}.{type}", Convert.FromBase64String(data));
			imageLink = $"{prefix}/{id}.{type}";

			return imageLink;
		}
		public static void DeleteImage(string path, string pathFileName)
		{
			try
			{
				File.Delete(path + pathFileName);
			}
			catch { }
		}
		public static string GetImageType(byte[] ImageData)
		{
			string result = string.Empty;

			try
			{
				bool Istype = false;
				for (int i = 0; i < ImageData.Length; i++)
				{
					if (Istype == false)
					{
						if ((char)ImageData[i] == '/')
						{
							Istype = true;
						}
					}
					else
					{
						if ((char)ImageData[i] == ';')
						{
							break;
						}
						else
						{
							result += (char)ImageData[i];
						}
					}
				}
			}
			catch
			{
				result = string.Empty;
			}

			return result;
		}
		public static bool IsImageType(string type)
		{
			bool result = false;

			foreach (var item in Constant.ListImageType)
			{
				if (type == item)
				{
					result = true;
					break;
				}
			}
			return result;
		}
		public static string GetImageBase64String(string imageData)
		{
			return imageData.Substring(imageData.IndexOf(",") + 1);
		}
		public static Image Base64ToImage(string base64String)
		{
			// Convert base 64 string to byte[]
			byte[] imageBytes = Convert.FromBase64String(base64String);
			// Convert byte[] to Image
			using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
			{
				Image image = Image.FromStream(ms, true);
				return image;
			}
		}
		public static Image ResizeImage(Image img, int width, int height)
		{
			Bitmap b = new Bitmap(width, height);
			Graphics g = Graphics.FromImage(b);

			g.DrawImage(img, 0, 0, width, height);
			g.Dispose();

			return b;
		}
		public static string SaveImages(string path, string prefix, string fileName, string data, string type, ImageSize[] imageSizes = null, bool isSaveMain = true, SaveImageCallback callback = null)
		{
			string mainImagePath = string.Empty;
			if (isSaveMain)
			{
				mainImagePath = SaveImage(path, prefix, fileName, type, data);
				using (var image = Image.FromFile(path + mainImagePath))
				{
					callback?.Invoke(mainImagePath, type, new ImageSize()
					{
						Width = (uint)image.Width,
						Height = (uint)image.Height
					});
				}
			}

			var img = Base64ToImage(data);
			foreach (var size in imageSizes)
			{
				try
				{
					var imageResized = ResizeImage(img, (int)size.Width, (int)size.Height);
					string pathImage = path + prefix;
					CommonHelper.CheckAndCreateDirectory(pathImage);
					imageResized.Save($"{pathImage}/{fileName}-{size.Width}x{size.Height}.{type}");
					callback?.Invoke($"{prefix}/{fileName}-{size.Width}x{size.Height}.{type}", type, size);
				}
				catch (Exception ex)
				{
					LogHelper.GetLogger().Error(string.Format("{0} - {1}", ex.StackTrace, ex.Message));
				}
			}

			return mainImagePath;
		}
	}
}
