﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common.Models
{
	public class ImageSize
	{
		public uint Width { set; get; }
		public uint Height { set; get; }
	}
}
