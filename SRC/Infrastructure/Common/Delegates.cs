﻿
using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public delegate void JobMethod();
    public delegate void ExceptionOnRunning(Exception ex);

	public delegate void SaveImageCallback(string fileName, string type, ImageSize size);
}
