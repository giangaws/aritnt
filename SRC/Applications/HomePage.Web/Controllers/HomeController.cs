﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HomePage.Web.Models;
using HomePage.Web.Infrastructure;
using Microsoft.Extensions.Localization;
using Homepage.UI.ViewModels;
using Web.Helpers;
using Common;
using Homepage.UI;
using Common.Exceptions;
using System.Net.Http;
using System.Net.Http.Headers;
using Common.Models;


using Homepage.UI.Interfaces;
using HomePage.Web.Common;
using Microsoft.AspNetCore.Mvc.ViewComponents;

namespace HomePage.Web.Controllers
{
	public class HomeController : BaseController
	{
		private readonly IProductHomepageQueries _productHomepageQueries;
		private readonly IContactHomepageRepository _contactHomepageRepository;
		private readonly IRetailerHomepageQueries _retailerHomepageQueries;
		private readonly IPostHomepageQueries _postHomepageQueries;
		private readonly IFaqHomepageQueries _faqHomepageQueries;
		private readonly ITopicHomepageQueries _topicHomepageQueries = null;
		private readonly IPageHomepageQueries _pageHomepageQueries = null;


		public HomeController(IStringLocalizer<SharedResource> localizer,
			IContactHomepageRepository contact,
			IProductHomepageQueries product,
			 IRetailerHomepageQueries about,
			 IPostHomepageQueries post,
			 IFaqHomepageQueries faq,
			 ITopicHomepageQueries topicHomepageQueries,
			 IPageHomepageQueries pageHomepageQueries) : base(localizer)
		{
			_contactHomepageRepository = contact;
			_productHomepageQueries = product;
			_retailerHomepageQueries = about;
			_postHomepageQueries = post;
			_faqHomepageQueries = faq;
			_topicHomepageQueries = topicHomepageQueries;
			_pageHomepageQueries = pageHomepageQueries;
		}

		public async Task<IActionResult> Index()
		{
			ViewData["HasSlider"] = "true";
			var product = (await _productHomepageQueries.GetProductOutstandingOfHomepage(CurrentLanguage))
				.OrderBy(x => Guid.NewGuid()).Take(4);
			if (product == null)
			{
				return Redirect("/error");
			}

			return View(product);
		}

		public async Task<IActionResult> About()
		{
			var product = await _retailerHomepageQueries.GetRetailerAsync();
			if (product == null)
			{
				return Redirect("/error");
			}

			return View(product);
		}

		public async Task<IActionResult> BuyWhere()
		{
			return View();
		}

		public async Task<IActionResult> CookingRecipe()
		{
			var recipeViewModel = new RecipesViewModel();
			recipeViewModel.DishPost = await _postHomepageQueries.GetAllPostByTopicId(22, lang: CurrentLanguage);

			recipeViewModel.SaucePost = await _postHomepageQueries.GetAllPostByTopicId(23, lang: CurrentLanguage);
			return View(recipeViewModel);
		}

		public async Task<IActionResult> ProductReturn()
		{
			return View();
		}

		public async Task<IActionResult> TheMedia()
		{
			return View();
		}

		public async Task<IActionResult> FarmerNetwork()
		{
			return View();
		}

		public async Task<IActionResult> Gifts()
		{
			return View();
		}
		
		public async Task<IActionResult> WeWork()
		{
			var allPost = await _postHomepageQueries.GetAllPostByTopicId(14, lang: CurrentLanguage);

			return View(allPost);
		}

		public async Task<IActionResult> Contact([Bind] ContactHomepageViewModel contact)
		{
			if (contact != null && !string.IsNullOrEmpty(contact.SenderName))
			{
				contact.CreatedDate = DateTime.Now;

				var rs = await _contactHomepageRepository.AddAsync(contact);
				if (rs > 0)
				{
					ViewData["alert_msg"] = "Thành công!";
					ViewData["alert_type"] = "success";
				}
				else
				{
					ViewData["alert_msg"] = "Thất bại!";
					ViewData["alert_type"] = "danger";
				}
			}

			return View();
		}

		public async Task<IActionResult> Faqs()
		{
			var model = await _faqHomepageQueries.GetAllFaq(condition: $"is_used = '1' and l.code ='{CurrentLanguage}' ");

			return View(model);
		}

		public async Task<ActionResult> Topic(int topicId)
		{
			var allPost = await _postHomepageQueries.GetAllPostByTopicId(topicId, lang: CurrentLanguage);

			return View(allPost);
		}

		public async Task<ActionResult> GetBlog(string name, int blogId)
		{
			var post = await _postHomepageQueries.GetPostById(blogId, lang: CurrentLanguage);

			return View(post);
		}

		public async Task<ActionResult> GetPageIsFooter()
		{
			var page = await _pageHomepageQueries.GetAllPage();

			return View(page);
		}

		public async Task<ActionResult> Page(int pageId)
		{
			PageHomepageViewModel page = await _pageHomepageQueries.GetPageByIdAsync(pageId);

			return View(page);
		}

		public ActionResult Trace()
		{
			return View();
		}

		public ActionResult Value()
		{
			return View();
		}

		[HttpGet]	
		public async Task<ActionResult> SearchAsync(string searchString)
		{
			var result = new List<ProductHomepageViewModel>();
			if (String.IsNullOrEmpty(searchString))
			{
				return RedirectToAction("Index", "Home");
			}
			else
			{
				var search = CommonHelper.SafePlainText(searchString);
				var products = await _productHomepageQueries.GetProductAsync(lang: CurrentLanguage);
				if (products == null)
				{
					return Redirect("/error");
				}
				result = products.Where(x => x.Name.ToUpper().Contains(search.ToUpper())).ToList();
				if (result.Count() == 0)
				{
					ViewBag.Notification = "Không tìm thấy kết quả.";
				}
			}


			return View(result);
		}

	}
}
