﻿using Homepage.UI.Interfaces;
using HomePage.Web.Infrastructure;
using HomePage.Web.Models;
using MDM.UI.Categories.Interfaces;
using MDM.UI.Categories.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomePage.Web.Views.Shared.Components.Footer
{
    public class HeaderViewComponent : BaseViewComponent
    {
        private readonly ICategoryQueries _categoryQueries;

        public HeaderViewComponent(
            IStringLocalizer<SharedResource> localizer,
            ICategoryQueries categoryQueries) : base(localizer)
        {
            _categoryQueries = categoryQueries;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            IEnumerable<CategoryViewModel> categories = (await _categoryQueries.GetsWithChild(CurrentLanguage))
                .OrderByDescending(x => x.Childs.Count())
                .ToList();

            return View(categories);
        }
    }
}
