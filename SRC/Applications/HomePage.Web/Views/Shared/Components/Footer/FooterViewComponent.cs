﻿using Homepage.UI.Interfaces;
using HomePage.Web.Infrastructure;
using HomePage.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomePage.Web.Views.Shared.Components.Footer
{
	public class FooterViewComponent : BaseViewComponent
	{
		private readonly IPostHomepageQueries _postHomepageQueries = null;
		private readonly ITopicHomepageQueries _topicHomepageQueries = null;
		private readonly IPageHomepageQueries _pageHomepageQueries = null;



		public FooterViewComponent(IPostHomepageQueries postHomepageQueries,
			ITopicHomepageQueries topicHomepageQueries,
			IStringLocalizer<SharedResource> localizer,
			IPageHomepageQueries pageHomepageQueries) : base(localizer)
		{
			this._postHomepageQueries = postHomepageQueries;
			_topicHomepageQueries = topicHomepageQueries;
			_pageHomepageQueries = pageHomepageQueries;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			var model = $"{DateTime.Now.ToString()}";
			var c = CurrentLanguage;

			var footerVm = new FooterViewModel();

			footerVm.Topics = await _topicHomepageQueries.GetAllTopic(condition: "t.is_footer = 1");
			footerVm.Pages = await _pageHomepageQueries.GetAllPage(condition: "p.is_footer = 1");
			return await Task.FromResult((IViewComponentResult)View("_FooterLayout", footerVm));
		}
	}
}
