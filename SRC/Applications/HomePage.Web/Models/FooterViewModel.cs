﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomePage.Web.Models
{
	public class FooterViewModel
	{
		public IEnumerable<Homepage.UI.ViewModels.TopicHomepageViewModel> Topics { get; set; }
		public IEnumerable<Homepage.UI.ViewModels.PageHomepageViewModel> Pages { get; set; }

	}
}
