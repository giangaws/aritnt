﻿using Homepage.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomePage.Web.Models
{
	public class RecipesViewModel
	{
		public IEnumerable<PostHomepageViewModel> DishPost { get; set; }
		public IEnumerable<PostHomepageViewModel> SaucePost { get; set; }
	}
}
