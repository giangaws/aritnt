﻿using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomePage.Web.Infrastructure
{
	public abstract class BaseViewComponent: ViewComponent
	{
		protected readonly IStringLocalizer<SharedResource> _localizer;
		public BaseViewComponent(IStringLocalizer<SharedResource> localizer)
		{
			_localizer = localizer;
		}

		private string _currentLanguage = string.Empty;
		protected string CurrentLanguage
		{
			get
			{
				if (!string.IsNullOrEmpty(_currentLanguage))
				{
					return _currentLanguage;
				}

				if (string.IsNullOrEmpty(_currentLanguage))
				{
					var feature = HttpContext.Features.Get<IRequestCultureFeature>();
					_currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
				}

				return _currentLanguage;
			}
		}
	}
}
