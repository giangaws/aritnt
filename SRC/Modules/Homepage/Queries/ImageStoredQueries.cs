﻿using Common.Models;
using DAL;
using Homepage.UI.Interfaces;
using Homepage.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homepage.Queries
{
	public class ImageStoredQueries : BaseQueries, IImageStoredQueries
	{
		public async Task<ImageStored> Get(int id)
		{
			string cmd = QueriesCreatingHelper.CreateQuerySelect<ImageStored>($"Id = {id}");

			return (await DALHelper.ExecuteQuery<ImageStored>(cmd, dbTransaction: DbTransaction, connection: DbConnection)).FirstOrDefault();
		}

		public async Task<ImageStored> Get(string code)
		{
			string cmd = QueriesCreatingHelper.CreateQuerySelect<ImageStored>($"Code = '{code}'");
			return (await DALHelper.ExecuteQuery<ImageStored>(cmd, dbTransaction: DbTransaction, connection: DbConnection)).FirstOrDefault();
		}

		public async Task<IEnumerable<ImageStored>> Gets(string conditions = "")
		{
			string cmd = QueriesCreatingHelper.CreateQuerySelect<ImageStored>(conditions);
			return await DALHelper.Query<ImageStored>(cmd, dbTransaction: DbTransaction, connection: DbConnection);
		}
	}
}
