﻿using System;
using System.Collections.Generic;
using System.Text;
using Web.Controllers;

namespace Homepage.Commands.ImageCommands
{
	public class DeleteCommand : BaseCommand<int>
	{
		public int ImageId { set; get; }
		public DeleteCommand(int imageId)
		{
			ImageId = imageId;
		}
	}
}
