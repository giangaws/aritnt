﻿using Common;
using Common.Exceptions;
using Common.Extensions;
using Common.Helpers;
using Homepage.UI;
using Homepage.UI.Interfaces;
using Homepage.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Web.Controllers;

namespace Homepage.Commands.ImageCommands
{
	public class UploadByOwnerCommandHandler : BaseCommandHandler<UploadByOwnerCommand, string>
	{
		private readonly IImageStoredQueries imageStoredQueries = null;
		private readonly IImageStoredRepository imageStoredRepository = null;
		public UploadByOwnerCommandHandler(IImageStoredQueries imageStoredQueries, IImageStoredRepository imageStoredRepository)
		{
			this.imageStoredQueries = imageStoredQueries;
			this.imageStoredRepository = imageStoredRepository;
		}
		public override async Task<string> HandleCommand(UploadByOwnerCommand request, CancellationToken cancellationToken)
		{
			const string TypeImage = "Post";

			if (!AppConst.ImageResizeList.ToList().Any(i => i.Feature == TypeImage))
			{
				throw new BusinessException("Common.WrongInput");
			}

			string type = ImageHelper.GetImageType(System.Text.Encoding.ASCII.GetBytes(request.ImageData));
			if (!ImageHelper.IsImageType(type))
			{
				throw new BusinessException("Common.Image.WrongType");
			}
			string base64StringData = ImageHelper.GetImageBase64String(request.ImageData);

			var fileName = CommonHelper.RemoveFileExtension(request.FileName);
			string urlFileName = fileName.FriendlyUrlFileName();

			string ImageMainPath = string.Empty;
			ImageStored imageStored = null;
			string endPrix = string.Empty;
			while ((imageStored = await imageStoredQueries.Get(urlFileName + endPrix)) != null)
			{
				if (string.IsNullOrEmpty(endPrix))
				{
					endPrix += "-1";
				}
				else
				{
					endPrix = "-" + (int.Parse(endPrix.Substring(1)) + 1);
				}
			}
			var code = Guid.NewGuid().ToString().Replace("-", "");
			ImageMainPath = ImageHelper.SaveImages(GlobalConfiguration.HomepageImagePath, $"{AppConst.ImageStoredPath}/{TypeImage}", urlFileName + endPrix, base64StringData, type, AppConst.ImageResizeList.ToList().First(i => i.Feature == TypeImage).Sizes, true, (filename, imageType, size) => {
				imageStoredRepository.Add(new ImageStored()
				{
					Code = urlFileName + endPrix,
					Feature = TypeImage,
					CreatedBy = request.LoginSession.Id,
					CreatedDate = DateTime.Now,
					PathFileName = filename,
					type = imageType,
					Width = (int)size.Width,
					Height = (int)size.Height
				}).Wait();
			});

			return ImageMainPath;
		}
	}
}
