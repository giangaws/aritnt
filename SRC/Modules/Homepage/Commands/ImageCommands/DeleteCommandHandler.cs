﻿using Common;
using Common.Helpers;
using Homepage.UI.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Web.Controllers;

namespace Homepage.Commands.ImageCommands
{
	public class DeleteCommandHandler : BaseCommandHandler<DeleteCommand, int>
	{
		private readonly IImageStoredQueries imageStoredQueries = null;
		private readonly IImageStoredRepository imageStoredRepository = null;
		public DeleteCommandHandler(IImageStoredQueries imageStoredQueries, IImageStoredRepository imageStoredRepository)
		{
			this.imageStoredQueries = imageStoredQueries;
			this.imageStoredRepository = imageStoredRepository;
		}
		public override async Task<int> HandleCommand(DeleteCommand request, CancellationToken cancellationToken)
		{
			var image = await imageStoredQueries.Get(request.ImageId);
			if (image != null)
			{
				await imageStoredRepository.Delete(request.ImageId);
				try
				{
					ImageHelper.DeleteImage(GlobalConfiguration.HomepageImagePath, image.PathFileName);
				}
				catch { }
			}
			return 0;
		}
	}
}
