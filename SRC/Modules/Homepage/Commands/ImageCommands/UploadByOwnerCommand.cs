﻿using System;
using System.Collections.Generic;
using System.Text;
using Web.Controllers;

namespace Homepage.Commands.ImageCommands
{
	public class UploadByOwnerCommand : BaseCommand<string>
	{
		public string ImageData { set; get; }
		public string FileName { set; get; }
		public UploadByOwnerCommand(string imageData, string fileName)
		{
			ImageData = imageData;
			FileName = fileName;
		}
	}
}
