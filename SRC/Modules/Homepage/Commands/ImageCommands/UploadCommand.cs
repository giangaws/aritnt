﻿using System;
using System.Collections.Generic;
using System.Text;
using Web.Controllers;

namespace Homepage.Commands.ImageCommands
{
	public class SignUpCommand : BaseCommand<string>
	{
		public string ImageData { set; get; }
		public string FileName { set; get; }
		public string TypeImage { set; get; }
		public SignUpCommand(string imageData, string fileName, string typeImage)
		{
			ImageData = imageData;
			FileName = fileName;
			TypeImage = typeImage;
		}
	}
}
