﻿using Homepage.Commands.ImageCommands;
using Homepage.UI.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.Attributes;
using Web.Controllers;
using Web.Controls;

namespace Homepage.Controllers
{

	[Route("api/[controller]")]
	[EnableCors("AllowSpecificOrigin")]
	public class ImageHomepageController : BaseController
	{
		private readonly IMediator mediator = null;
		private readonly IImageStoredQueries imageStoredQueries = null;
		public ImageHomepageController(IMediator mediator = null, IImageStoredQueries imageStoredQueries = null)
		{
			this.mediator = mediator;
			this.imageStoredQueries = imageStoredQueries;
		}

		[HttpGet]
		[Route("gets")]
		public async Task<APIResult> Gets()
		{
			var rs = await imageStoredQueries.Gets();
			return new APIResult()
			{
				Result = 0,
				Data = rs
			};
		}

		[HttpGet]
		[Route("gets/by-feature")]
		public async Task<APIResult> GetsByFeature(string featureName)
		{
			var rs = await imageStoredQueries.Gets($"Feature = '{featureName}'");
			return new APIResult()
			{
				Result = 0,
				Data = rs
			};
		}

		[HttpPost]
		[Route("upload/by-owner")]
		[AuthorizeUser()]
		public async Task<APIResult> UpLoad([FromBody]UploadByOwnerCommand command)
		{
			var rs = await mediator.Send(command);
			return new APIResult()
			{
				Result = 0,
				Data = rs
			};
		}

		[HttpPost]
		[Route("upload")]
		[AuthorizeUser()]
		public async Task<APIResult> UpLoad([FromBody]SignUpCommand command)
		{
			var rs = await mediator.Send(command);
			return new APIResult()
			{
				Result = 0,
				Data = rs
			};
		}

		[HttpPost]
		[Route("delete")]
		[AuthorizeUser()]
		public async Task<APIResult> Delete([FromBody]DeleteCommand command)
		{
			var rs = await mediator.Send(command);
			return new APIResult()
			{
				Result = 0,
				Data = rs
			};
		}
	}
}
