﻿using Common.Models;
using DAL;
using Homepage.UI.Interfaces;
using Homepage.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homepage.Repositories
{
	public class ImageStoredRepository : BaseRepository, IImageStoredRepository
	{
		public async Task<int> Add(ImageStored image)
		{
			string cmd = QueriesCreatingHelper.CreateQueryInsert(image);
			cmd += ";SELECT LAST_INSERT_ID();";
			return (await DALHelper.ExecuteQuery<int>(cmd, dbTransaction: DbTransaction, connection: DbConnection)).First();
		}

		public async Task<int> Delete(int imageId)
		{
			string cmd = $@"DELETE FROM `image_stored`
                            WHERE Id = '{imageId}';";
			return (await DALHelper.Execute(cmd, dbTransaction: DbTransaction, connection: DbConnection)) > 0 ? 0 : -1;
		}
	}
}
