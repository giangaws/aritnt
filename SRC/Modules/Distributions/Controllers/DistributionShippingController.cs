﻿using MDM.UI.Common.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Web.Attributes;
using Web.Controllers;
using Web.Controls;

namespace Distributions.Distributions.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    public class DistributionShippingController : BaseController
    {
        private readonly IMediator mediator = null;
        private readonly ICFShippingQueries fDShippingQueries = null;
        public DistributionShippingController(IMediator mediator, ICFShippingQueries fDShippingQueries)
        {
            this.mediator = mediator;
            this.fDShippingQueries = fDShippingQueries;
        }

        [HttpGet]
        [Route("gets/un-completed")]
        [AuthorizeUser("DeliverySupervisor")]
        public async Task<APIResult> Gets(int distributionId)
        {
            return new APIResult()
            {
                Result = 0,
                Data = await fDShippingQueries.Gets($"distribution_id = {distributionId} and status_id = {(int)UI.TripStatuses.OnTriped}")
            };
        }

        [HttpGet]
        [Route("get")]
        [AuthorizeUser("DeliverySupervisor")]
        public async Task<APIResult> Get(long id)
        {
            return new APIResult()
            {
                Result = 0,
                Data = await fDShippingQueries.Get(id)
            };
        }

        [HttpGet]
        [Route("gets/order")]
        [AuthorizeUser("DeliverySupervisor")]
        public async Task<APIResult> GetOrders(long shippingId)
        {
            return new APIResult()
            {
                Result = 0,
                Data = await fDShippingQueries.GetItems(shippingId)
            };
        }

        //[HttpPost]
        //[Route("update/status")]
        //[AuthorizeUser("DeliverySupervisor")]
        //public async Task<APIResult> Delete([FromBody]UpdateStatusCommand command)
        //{
        //    var rs = await mediator.Send(command);
        //    return new APIResult()
        //    {
        //        Result = rs
        //    };
        //}
    }
}
