﻿using Fulfillments.UI.Interfaces;
using MDM.UI.Fulfillments.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.Attributes;
using Web.Controllers;
using Web.Controls;

namespace Fulfillments.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    public class FulfillmentCollectionController : BaseController
    {

        private readonly IMediator mediator = null;
        private readonly IFulfillmentQueries fulfillmentQueries = null;
        private readonly IFulfillmentCollectionQueries queries = null;

        public FulfillmentCollectionController(IMediator mediator, IFulfillmentQueries fulfillmentQueries,
            IFulfillmentCollectionQueries queries)
        {
            this.mediator = mediator;
            this.fulfillmentQueries = fulfillmentQueries;
            this.queries = queries;
        }

        [HttpGet]
        [Route("get")]
        [AuthorizeUser("Fulfillmentor")]
        public async Task<APIResult> GetFulfillmentById(string id)
        {
            return new APIResult()
            {
                Result = 0,
                Data = await queries.GetFulfillmentCollectionById(id)
            };
        }

        [HttpGet]
        [Route("get/collection")]
        [AuthorizeUser("Fulfillmentor")]
        public async Task<APIResult> GetCollection()
        {
            return new APIResult()
            {
                Result = 0,
                Data = await queries.GetCollection()
            };
        }
        [HttpGet]
        [Route("get/odercollection")]
        [AuthorizeUser("Fulfillmentor")]
        public async Task<APIResult> GetFromCollection()
        {
            return new APIResult()
            {
                Result = 0,
                Data = await queries.GetOrderFromCollection()
            };
        }
    }
}
