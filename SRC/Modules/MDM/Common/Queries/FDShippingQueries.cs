﻿using Common.Models;
using DAL;
using MDM.UI.Common.Interfaces;
using MDM.UI.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDM.Common.Queries
{
    public class FDShippingQueries : BaseQueries, IFDShippingQueries
    {
        private const string ShippingCodeFormat = "S{0}";
        public async Task<string> GenarateCode()
        {
            string code = string.Empty;
            var previousCode = await DALHelper.ExecuteScadar<string>("SELECT max(code) FROM `cf_shipping`");
            if (previousCode == null)
            {
                code = ShippingCodeFormat.Replace("{0}", 1.ToString("000000000"));
            }
            else
            {
                code = ShippingCodeFormat.Replace("{0}", (Int32.Parse(previousCode.Substring(1, 9)) + 1).ToString("000000000"));
            }

            return code;
        }

        public async Task<FDShipping> Get(long shippingId)
        {
            return (await DALHelper.Query<FDShipping>($"SELECT * FROM `cf_shipping` WHERE `id` = {shippingId} AND is_deleted = 0", dbTransaction: DbTransaction, connection: DbConnection)).FirstOrDefault();
        }

        public async Task<FDShippingItem> GetItem(long shippingItemId)
        {
            string cmd = $"SELECT * FROM `cf_shipping_item` WHERE id = {shippingItemId}";
            return (await DALHelper.Query<FDShippingItem>(cmd, dbTransaction: DbTransaction, connection: DbConnection)).FirstOrDefault();
        }

        public async Task<IEnumerable<FDShippingItem>> GetItems(long shippingId)
        {
            string cmd = $"SELECT * FROM `cf_shipping_item` WHERE cf_shipping_id = {shippingId}";
            return await DALHelper.Query<FDShippingItem>(cmd, dbTransaction: DbTransaction, connection: DbConnection);
        }

        public async Task<IEnumerable<FDShippingItem>> GetItems(string condition = "")
        {
            string cmd = "SELECT * FROM `cf_shipping_item`";
            if (!string.IsNullOrEmpty(condition))
            {
                cmd += " AND " + condition;
            }
            return await DALHelper.Query<FDShippingItem>(cmd, dbTransaction: DbTransaction, connection: DbConnection);
        }

        public async Task<IEnumerable<FDShipping>> Gets(string condition = "")
        {
            string cmd = "SELECT * FROM `cf_shipping` WHERE is_deleted = 0";
            if (!string.IsNullOrEmpty(condition))
            {
                cmd += " AND " + condition;
            }
            return await DALHelper.Query<FDShipping>(cmd, dbTransaction: DbTransaction, connection: DbConnection);
        }
    }
}
