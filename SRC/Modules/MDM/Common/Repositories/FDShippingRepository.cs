﻿using Common.Models;
using DAL;
using MDM.UI.Common.Interfaces;
using MDM.UI.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDM.Common.Repositories
{
    public class FDShippingRepository : BaseRepository, IFDShippingRepository
    {
        public async Task<int> Add(FDShipping shipping)
        {
            var cmd = QueriesCreatingHelper.CreateQueryInsert(shipping);
            cmd += ";SELECT LAST_INSERT_ID();";
            return (await DALHelper.ExecuteQuery<int>(cmd, dbTransaction: DbTransaction, connection: DbConnection)).First();
        }

        public async Task<int> AddItem(FDShippingItem shippingItem)
        {
            var cmd = QueriesCreatingHelper.CreateQueryInsert(shippingItem);
            cmd += ";SELECT LAST_INSERT_ID();";
            return (await DALHelper.ExecuteQuery<int>(cmd, dbTransaction: DbTransaction, connection: DbConnection)).First();
        }

        public async Task<int> Delete(FDShipping shipping)
        {
            var cmd = $@"UPDATE `fd_shipping`
                         SET
                         `is_deleted` = 1,
                         `modified_by` = {shipping.ModifiedBy},
                         `modified_date` = '{shipping.ModifiedDate?.ToString("yyyyMMddHHmmss")}'
                         WHERE `id` = {shipping.Id}";
            var rs = await DALHelper.Execute(cmd, dbTransaction: DbTransaction, connection: DbConnection);
            return rs == 0 ? -1 : 0;
        }

        public async Task<int> DeleteItem(long fdShippingItemId)
        {
            var cmd = $@"DELETE FROM `fd_shipping_item` WHERE `id` = {fdShippingItemId}";
            var rs = await DALHelper.Execute(cmd, dbTransaction: DbTransaction, connection: DbConnection);
            return rs == 0 ? -1 : 0;
        }

        public async Task<int> DeleteItems(long fdShippingId)
        {
            var cmd = $@"DELETE FROM `fd_shipping_item` WHERE `fd_shipping_id` = {fdShippingId}";
            var rs = await DALHelper.Execute(cmd, dbTransaction: DbTransaction, connection: DbConnection);
            return rs == 0 ? -1 : 0;
        }

        public async Task<int> Update(FDShipping shipping)
        {
            var cmd = QueriesCreatingHelper.CreateQueryUpdate(shipping);
            var rs = await DALHelper.Execute(cmd, dbTransaction: DbTransaction, connection: DbConnection);
            return rs == 0 ? -1 : 0;
        }

        public async Task<int> UpdateItem(FDShippingItem shippingItem)
        {
            var cmd = QueriesCreatingHelper.CreateQueryUpdate(shippingItem);
            var rs = await DALHelper.Execute(cmd, dbTransaction: DbTransaction, connection: DbConnection);
            return rs == 0 ? -1 : 0;
        }
    }
}
