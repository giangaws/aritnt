﻿using Order.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Web.Controllers;

namespace Order.Commands.RetailerOrder
{
    public class UpdateCollectionCommand : BaseCommand<int>
    {
        public long OrderId { set; get; }
        public int CollectionId { set; get; }
        public UpdateCollectionCommand(long orderId, int collectionId)
        {
            OrderId = orderId;
            CollectionId = collectionId;
        }
    }
}
