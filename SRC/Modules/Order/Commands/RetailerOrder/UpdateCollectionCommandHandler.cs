﻿using Common;
using Common.Exceptions;
using Common.Models;
using DAL;
using MDM.UI.Retailers.Models;
using Order.UI;
using Order.UI.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Web.Controllers;
using Web.Helpers;

namespace Order.Commands.RetailerOrder
{
    public class UpdateCollectionCommandHandler : BaseCommandHandler<UpdateCollectionCommand, int>
    {
        private readonly IRetailerOrderRepository retailerOrderRepository = null;
        private readonly IRetailerOrderQueries retailerOrderQueries = null;
        public UpdateCollectionCommandHandler(IRetailerOrderRepository retailerOrderRepository,
                                    IRetailerOrderQueries retailerOrderQueries)
        {
            this.retailerOrderRepository = retailerOrderRepository;
            this.retailerOrderQueries = retailerOrderQueries;
        }
        public override async Task<int> HandleCommand(UpdateCollectionCommand request, CancellationToken cancellationToken)
        {
            if (request.OrderId == 0)
            {
                throw new BusinessException("Order.NotExisted");
            }

            var order = await retailerOrderQueries.Get(request.OrderId);

            if(order == null)
            {
                throw new BusinessException("Order.NotExisted");
            }

            if(order.StatusId != (int)RetailerOrderStatuses.FarmerOrdered && request.CollectionId != order.CollectionId)
            {
                throw new BusinessException("Order.WrongStep");
            }

            if (order.CollectionId == null)
            {
                order.CollectionId = request.CollectionId;
                order.StatusId = (int)RetailerOrderStatuses.InCollection;
                order = UpdateBuild(order, request.LoginSession);
                var rs = await retailerOrderRepository.Update(order);

                await retailerOrderRepository.AddAudit(new UI.Models.RetailerOrderAudit()
                {
                    RetailerId = order.RetailerId,
                    RetailerOrderId = order.Id,
                    RetailerOrderItemId = null,
                    StatusId = order.StatusId,
                    CreatedBy = request.LoginSession.Id,
                    CreatedDate = DateTime.Now,
                    Note = string.Empty
                });

                return rs;
            }

            return 0;
        }

        public async override Task<int> HandleCommandTransaction(UpdateCollectionCommand request, CachingCommandTransactionModel trans, CancellationToken cancellationToken)
        {
            if (request.OrderId == 0)
            {
                throw new BusinessException("Order.NotExisted");
            }

            retailerOrderQueries.JoinTransaction(trans.Connection, trans.Transaction);
            retailerOrderRepository.JoinTransaction(trans.Connection, trans.Transaction);

            var order = await retailerOrderQueries.Get(request.OrderId);

            if (order == null)
            {
                throw new BusinessException("Order.NotExisted");
            }

            if (order.StatusId != (int)RetailerOrderStatuses.FarmerOrdered && request.CollectionId != order.CollectionId)
            {
                throw new BusinessException("Order.WrongStep");
            }

            if (order.CollectionId == null)
            {
                order.CollectionId = request.CollectionId;
                order.StatusId = (int)RetailerOrderStatuses.InCollection;
                order = UpdateBuild(order, request.LoginSession);
                var rs = await retailerOrderRepository.Update(order);

                await retailerOrderRepository.AddAudit(new UI.Models.RetailerOrderAudit()
                {
                    RetailerId = order.RetailerId,
                    RetailerOrderId = order.Id,
                    RetailerOrderItemId = null,
                    StatusId = order.StatusId,
                    CreatedBy = request.LoginSession.Id,
                    CreatedDate = DateTime.Now,
                    Note = string.Empty
                });

                return rs;
            }

            return 0;
        }
    }
}
