﻿using Common.Models;
using Homepage.UI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Homepage.UI
{
	public class AppConst
	{
		public static readonly FeatureImageSize[] ImageResizeList =
		{
			new FeatureImageSize()
			{
				Feature = "Post",
				Sizes = new ImageSize[0]
			},
			new FeatureImageSize()
			{
				Feature = "Page",
				Sizes = new ImageSize[0]
			},
			new FeatureImageSize()
			{
				Feature = "Common",
				Sizes = new ImageSize[0]
			}
		};
		public const string ImageStoredPath = "/Images";
	}
}
