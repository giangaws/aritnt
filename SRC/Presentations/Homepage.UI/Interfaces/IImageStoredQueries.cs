﻿using Homepage.UI.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Homepage.UI.Interfaces
{
	public interface IImageStoredQueries
	{
		Task<IEnumerable<ImageStored>> Gets(string conditions = "");
		Task<ImageStored> Get(int id);
		Task<ImageStored> Get(string pathFileName);
	}
}
