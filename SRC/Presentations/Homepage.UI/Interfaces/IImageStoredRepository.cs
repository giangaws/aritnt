﻿using Homepage.UI.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Homepage.UI.Interfaces
{
	public interface IImageStoredRepository
	{
		Task<int> Add(ImageStored image);
		Task<int> Delete(int imageId);
	}
}
