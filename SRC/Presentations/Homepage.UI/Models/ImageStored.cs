﻿using Common.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Homepage.UI.Models
{
	[Table("image_stored")]
	public class ImageStored
	{
		[Key]
		[Identity]
		[Column("id")]
		public int Id { set; get; }
		[Column("code")]
		[Required]
		public string Code { set; get; }
		[Column("feature")]
		[Required]
		public string Feature { set; get; }
		[Column("pathfilename")]
		[Required]
		public string PathFileName { set; get; }
		[Column("type")]
		[Required]
		public string type { set; get; }
		[Column("width")]
		[Required]
		public int Width { set; get; }
		[Column("height")]
		[Required]
		public int Height { set; get; }
		[Column("creadted_date")]
		[Required]
		public DateTime CreatedDate { set; get; }
		[Column("created_by")]
		[Required]
		public int CreatedBy { set; get; }
	}
}
