﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Homepage.UI.Models
{
	public class FeatureImageSize
	{
		public string Feature { set; get; }
		public ImageSize[] Sizes { set; get; }
	}
}
