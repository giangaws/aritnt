﻿using Common.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MDM.UI.Common.Models
{
    [Table("fd_shipping_item")]
    public class FDShippingItem
    {
        [Key]
        [Identity]
        [Column("id")]
        public long Id { set; get; }

        [Column("cf_shipping_id")]
        public long ShippingId { set; get; }

        [Column("retailer_order_id")]
        public long OrderId { set; get; }


        [Column("is_received")]
        public bool IsReceiced { set; get; }
    }
}
