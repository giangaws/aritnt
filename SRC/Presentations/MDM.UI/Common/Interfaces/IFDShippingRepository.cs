﻿using Common.Interfaces;
using MDM.UI.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MDM.UI.Common.Interfaces
{
    public interface IFDShippingRepository : IBaseRepository
    {
        Task<int> Add(FDShipping shipping);
        Task<int> Update(FDShipping shipping);
        Task<int> Delete(FDShipping shipping);

        Task<int> AddItem(FDShippingItem shippingItem);
        Task<int> UpdateItem(FDShippingItem shippingItem);
        Task<int> DeleteItem(long fdShippingItemId);
        Task<int> DeleteItems(long fdShippingId);
    }
}
