﻿using Common.Interfaces;
using MDM.UI.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MDM.UI.Common.Interfaces
{
    public interface IFDShippingQueries : IBaseQueries
    {
        Task<string> GenarateCode();
        Task<FDShipping> Get(long shippingId);
        Task<IEnumerable<FDShipping>> Gets(string condition = "");

        Task<IEnumerable<FDShippingItem>> GetItems(long shippingId);
        Task<IEnumerable<FDShippingItem>> GetItems(string condition = "");
        Task<FDShippingItem> GetItem(long shippingItemId);
    }
}
