
import { AppBaseComponent } from 'src/core/basecommon/app-base.component';

import { AppConsts } from 'src/core/constant/AppConsts';
import { ResultCode } from 'src/core/constant/AppEnums';
import { FuncHelper } from 'src/core/helpers/function-helper';
import { ImageStored, ImageService } from './image.service';
import { Component, Injector } from '@angular/core';

@Component({
  selector: 'image-stored',
  templateUrl: './image-stored.component.html',
  styleUrls: ['./image-stored.component.css']
})
export class ImageStoredComponent extends AppBaseComponent {

  private images: ImageStored[] = [];
  private features: string[] = [];
  private chooseFeature: string = '';
  private chooseImageId: number = 0;

  private imageServer: string = AppConsts.imageDataUrl;

  constructor(
    injector: Injector,
    private imgSvc: ImageService
  ) {
    super(injector);

    this.features = this.imgSvc.getListFeature();
    if(this.features.length > 0) {
      this.chooseFeature = this.features[0];
    }
    if(this.chooseFeature != '') {
      this.loadDatasource();
    }
  }

  loadDatasource(callback: () => void = null) {
    this.imgSvc.getsByFeature(this.chooseFeature).subscribe((result) => {
      if (result.result == ResultCode.Success) {

        this.images = result.data;

        if (FuncHelper.isFunction(callback))
          callback();
      }
      else {
        this.showError(result.errorMessage);
      }
    });
  }

  changeEvent(fileInput: any, component: ImageStoredComponent) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e: any) {
        component.imgSvc.upload(e.target.result, fileInput.target.files[0].name, component.chooseFeature).subscribe(rs => {
          if(rs.result == ResultCode.Success) {
            component.loadDatasource();
          }
          else{
            component.showError(rs.errorMessage);
          }
        });

      }
      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  private delete() {
    this.showYesNoQuestion(this.lang.instant("Common.DeleteQuestion"), () => {
      this.imgSvc.delete(this.chooseImageId).subscribe(rs => {
        if(rs.result == ResultCode.Success) {
          this.showSuccess("Common.DeleteSuccess");
          this.chooseImageId = 0;
          this.loadDatasource();
        }
        else 
        {
          this.showError(rs.errorMessage);
        }
      });
    });
  }

  private chooseImage(id: number){
    this.chooseImageId = id;
  }

  private onFeatureChange(feature: string) {
    this.chooseFeature = feature;
    this.chooseImageId = 0;
    this.loadDatasource();
  }

  private getImage(id) {
    if(this.images == null) return null;
    return this.images.find(i => i.id == id);
  }
}
