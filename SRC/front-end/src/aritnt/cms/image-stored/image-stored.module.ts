import { ImageStoredRoutingModule } from './image-stored-routing.module';
import { ImageStoredComponent } from './image-stored.component';
import { ImageService } from './image.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [
    CommonModule,
    ImageStoredRoutingModule
  ],
  providers: [ImageService],
  declarations: [ImageStoredComponent]
})
export class ImageStoredModule { }
