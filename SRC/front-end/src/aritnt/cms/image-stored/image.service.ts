
import { AuthenticService } from 'src/core/Authentication/authentic.service';

import { ResultModel } from 'src/core/models/http.model';

import { AppConsts } from 'src/core/constant/AppConsts';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export class ImageStored {
    id: number;
    code: string;
    pathFileName: string;
    type: string;
    width: string = '<Width>';
    height: string = '<Height>';
    createdDate: string;
}

@Injectable()
export class ImageService {
    constructor(
        private http: HttpClient,
        private authenticSvc: AuthenticService) { }

    private urlGets: string = '/api/ImageHomepage/gets';
    private urlGetsByFeature: string = '/api/ImageHomepage/gets/by-feature';
    private urlUpload: string = '/api/ImageHomepage/upload';
    private urlUploadByOwner: string = '/api/ImageHomepage/upload/by-owner';
    private urlDelete: string = '/api/ImageHomepage/delete';

    gets(): Observable<ResultModel<ImageStored[]>> {
        debugger
        return this.http.get<ResultModel<ImageStored[]>>(`${AppConsts.remoteServiceBaseUrl}${this.urlGets}`, this.authenticSvc.getHttpHeader())
    }

    getsByFeature(featureName : string): Observable<ResultModel<ImageStored[]>> {
        debugger
        return this.http.get<ResultModel<ImageStored[]>>(`${AppConsts.remoteServiceBaseUrl}${this.urlGetsByFeature}?featureName=${featureName}`, this.authenticSvc.getHttpHeader())
    }

    upload(imageData: string, fileName: string, typeImage: string): Observable<ResultModel<string>> {
        debugger
        return this.http.post<ResultModel<string>>(`${AppConsts.remoteServiceBaseUrl}${this.urlUpload}`, { imageData, fileName, typeImage }, this.authenticSvc.getHttpHeader());
    }

    uploadByOwner(imageData: string, fileName: string): Observable<ResultModel<string>> {
        debugger
        return this.http.post<ResultModel<string>>(`${AppConsts.remoteServiceBaseUrl}${this.urlUploadByOwner}`, { imageData, fileName }, this.authenticSvc.getHttpHeader());
    }

    delete(imageId: number): Observable<ResultModel<string>> {
        debugger
        return this.http.post<ResultModel<string>>(`${AppConsts.remoteServiceBaseUrl}${this.urlDelete}`, { imageId }, this.authenticSvc.getHttpHeader());
    }

    getListFeature(): string[] {
        return [
            "Post",
            "Page",
            "Common"
        ];
    }
}
