import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceivingRoutingModule } from './receiving-routing.module';
import { ReceivingComponent } from './receiving.component';
import { ReceivingService } from './receiving.service';
import { EmployeeService } from '../common/services/employee.service';
import { UserService } from '../common/services/user.service'
import { VehicleService } from '../common/services/vehicle.service';
import { LocationService } from '../common/services/location.service';
import { DistributionService } from '../common/services/distribution.service';
import { DistributionEmployeeService } from '../common/services/distribution-employee.service';
import { 
  DxDataGridModule,
  DxButtonModule,
  DxSelectBoxModule
} from 'devextreme-angular';
import { FormsModule } from '@angular/forms';
import { FulfillmentService } from 'src/aritnt/administrator/fulfillment/fulfillment.service';

@NgModule({
  imports: [
    CommonModule,
    DxDataGridModule,
    DxButtonModule,
    DxSelectBoxModule,
    ReceivingRoutingModule,
    FormsModule
  ],
  declarations: [ReceivingComponent],
  providers: [ReceivingService, FulfillmentService, EmployeeService, VehicleService, LocationService, DistributionService]
})
export class ReceivingModule { }
