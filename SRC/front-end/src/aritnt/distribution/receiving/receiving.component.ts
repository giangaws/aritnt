import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { AppBaseComponent } from 'src/core/basecommon/app-base.component';
import { ReceivingService, Receiving, TripStatus } from './receiving.service';
import { ResultCode } from 'src/core/constant/AppEnums';
import { Employee, EmployeeService } from '../common/services/employee.service';
import { User, UserService } from '../common/services/user.service';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import { Vehicle, VehicleService } from '../common/services/vehicle.service';
import { LocationService } from '../common/services/location.service';
import { DistributionService, Distribution } from '../common/services/distribution.service';
import { DistributionEmployeeService } from '../common/services/distribution-employee.service';
import { FulfillmentService, Fulfillment } from 'src/aritnt/administrator/fulfillment/fulfillment.service';

@Component({
  selector: 'receiving',
  templateUrl: './receiving.component.html',
  styleUrls: ['./receiving.component.css']
})
export class ReceivingComponent extends AppBaseComponent implements OnInit {
  @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
  selectedRows: number[];

  private distributions: Distribution[] = [];
  private chooseDistribution: Distribution;

  private receivings: Receiving[] = [];
  private fulfillments: Fulfillment[] = [];
  private employees: Employee[] = [];
  private statuses: TripStatus[] = [];
  private vehicles: Vehicle[] = [];

  private now = new Date(Date.now());

  constructor(
    injector: Injector,
    private receivingSvc: ReceivingService,
    private empSvc: EmployeeService,
    private fulSvc: FulfillmentService,
    private vehiSvc: VehicleService,
    private locSvc: LocationService,
    private disSvc: DistributionService,
  ) {
    super(injector);

    this.loadSource();
  }

  private async loadSource() {
    this.vehiSvc.gets().subscribe(rs => {
      if (rs.result == ResultCode.Success) {
        this.vehicles = rs.data;
      }
    });

    let rsStatuses = await this.receivingSvc.getStatuses().toPromise();
    if (rsStatuses.result == ResultCode.Success) {
      this.statuses = rsStatuses.data;
    }

    this.fulSvc.gets().subscribe(rs => {
      if (rs.result == ResultCode.Success) {
        this.fulfillments = rs.data;
      }
    });

    this.empSvc.gets().subscribe(rs => {
      if(rs.result == ResultCode.Success) {
        this.employees = rs.data;
      }
    });

    this.loadMainSource();
  }

  private loadMainSource() {
    this.disSvc.getByOwners().subscribe(disRs => {
      if (disRs.result == ResultCode.Success) {
        this.distributions = disRs.data;
      }
    });
  }

  private loadReceivingSource(distributionId: number) {
    this.receivings = null;
    this.receivingSvc.gets(distributionId).subscribe(rs => {
      if(rs.result == ResultCode.Success) {
        this.receivings = rs.data;
      }
      else {
        this.showError(rs.errorMessage);
      }
    }); 
  }

  private distributionChanged(event) {
    if(event.value == null) {
      return;
    }
    this.loadReceivingSource(event.value.id);
    this.dataGrid.instance.collapseAll(-1);
    this.dataGrid.instance.cancelEditData();
  }

  create() {
    this.dataGrid.instance.addRow();
  }

  update() {
    if (this.selectedRows != null && this.selectedRows.length == 1) {
      var index = this.dataGrid.instance.getRowIndexByKey(this.selectedRows[0]);
      console.log(index);
      this.dataGrid.instance.editRow(this.dataGrid.instance.getRowIndexByKey(this.selectedRows[0]));
    }
  }


  private async confirm() {
    if (this.selectedRows != null && this.selectedRows.length == 1) {
      let receiving = this.receivings.find(t => t.id == this.selectedRows[0]);
      if (receiving != null) {
        if(receiving.shipperId == null) {
          this.showError("Distribution.Receiving.RequiredDeliveryMan");
          return;
        }
        if(receiving.distributionId == null) {
          this.showError("Distribution.Receiving.RequiredDriver");
          return;
        }
        if(receiving.vehicleId == null) {
          this.showError("Distribution.Receiving.RequiredVehicle");
          return;
        }
        // let rs = await this.receivingSvc.changeStatus(this.selectedRows[0], 2).toPromise();
        // if (rs.result == ResultCode.Success) {
        //   this.showSuccess('Common.UpdateSuccess');
        //   receiving.statusId = 2;
        // }
        // else {
        //   this.showError(rs.errorMessage);
        // }
      }
    }
  }

  private async onReceivingExpanding(event) {
    let id = event.key;
    let receiving = this.receivings.find(t => t.id == id);
    if (receiving.orders == null) {
      receiving.orders = [];
      this.receivingSvc.getOrders(this.chooseDistribution.id, receiving.id).subscribe(async rs => {
        if (rs.result == ResultCode.Success) {
          receiving.orders = rs.data;
        }
      });
    }
  }

  getStatus(id: number) {
    return this.statuses.find(s => s.id == id);
  }

  getReceiving(id: number) {
    if(this.receivings == null) {
      return null;
    }
    return this.receivings.find(t => t.id == id);
  }
}
