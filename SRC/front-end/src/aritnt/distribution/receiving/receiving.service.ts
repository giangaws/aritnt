import { Injectable } from '@angular/core';
import { ResultModel } from 'src/core/models/http.model';
import { Observable } from 'rxjs';
import { AuthenticService } from 'src/core/Authentication/authentic.service';
import { AppConsts } from 'src/core/constant/AppConsts';
import { HttpClient } from '@angular/common/http';
import { Vehicle } from '../common/services/vehicle.service';
import { Employee } from '../common/services/employee.service';
import { Product } from '../common/services/product.service';
import { UoM } from '../common/services/uom.service';
import { RetailerLocation } from '../common/services/location.service';
import { RetailerOrder } from 'src/aritnt/administrator/retailer/retailer-order.service';

export class Receiving {
  id: number = 0;
  code: string = '';
  fulfillmentId: number = null;
  distributionId: number = null;
  shipperId: number = null;
  vehicleId: number = null;
  deliveryDate: Date;
  statusId: number = null;

  orders: RetailerOrder[] = null;
}

export class TripStatus {
  id: number = 0;
  name: string = '';
  description: string = '';
  flagColor: string = '';
}

@Injectable()
export class ReceivingService {
  constructor(
    private http: HttpClient,
    private authenticSvc: AuthenticService) { }

  private urlReceiving: string = '/api/DistributionShipping/get';
  private urlReceivings: string = '/api/DistributionShipping/gets/un-completed';
  private urlOrders: string = '/api/DistributionShipping/gets/order';
  private urlTripStatueses: string = '/api/trip/gets/status';
  private urlUpdateStatusReceiving: string = '/api/receiving/update/status';

  get(receivingId: number): Observable<ResultModel<Receiving>> {
    return this.http.get<ResultModel<Receiving>>(`${AppConsts.remoteServiceBaseUrl}${this.urlReceiving}?receivingId=${receivingId}`, this.authenticSvc.getHttpHeader());
  }

  gets(distributionId: number): Observable<ResultModel<Receiving[]>> {
    return this.http.get<ResultModel<Receiving[]>>(`${AppConsts.remoteServiceBaseUrl}${this.urlReceivings}?distributionId=${distributionId}`, this.authenticSvc.getHttpHeader());
  }

  getOrders(distributionId: number, receivingId: number): Observable<ResultModel<RetailerOrder[]>> {
    return this.http.get<ResultModel<RetailerOrder[]>>(`${AppConsts.remoteServiceBaseUrl}${this.urlOrders}?distributionId=${distributionId}&receivingId=${receivingId}`, this.authenticSvc.getHttpHeader());
  }

  getStatuses() : Observable<ResultModel<TripStatus[]>> {
    return this.http.get<ResultModel<TripStatus[]>>(`${AppConsts.remoteServiceBaseUrl}${this.urlTripStatueses}`, this.authenticSvc.getHttpHeader());
  }
}
