import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { 
  DxDataGridModule, 
  DxButtonModule, 
  DxSelectBoxModule, 
  DxTextBoxModule, 
  DxValidatorModule,
  DxPopupModule,
  DxDateBoxModule,
} from 'devextreme-angular';
import { FormsModule } from '@angular/forms';
import { OrderRoutingModule } from './fc-routing.module';
import { FCMasterComponent } from './master/master.component';
import { FCDetailComponent } from './detail/detail.component';
import { InventoryService } from 'src/aritnt/administrator/common/services/inventory.service';
import { OrderService } from 'src/aritnt/retailer/order/order.service';
import {FulfillmentService } from '../fulfillment.service';
import { ProductService } from 'src/aritnt/administrator/product/product.service';
import { UoMService } from 'src/aritnt/administrator/common/services/uom.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OrderRoutingModule,
    DxDataGridModule,
    DxButtonModule,
    DxSelectBoxModule,
    DxTextBoxModule,
    DxValidatorModule,
    DxPopupModule,
    DxDateBoxModule,
    
  ],
  declarations: [
    FCMasterComponent,
    FCDetailComponent,
  ],
  providers: [
    InventoryService,
    OrderService,
    FulfillmentService,
    ProductService,
    UoMService
  ]
})
export class FCModule { }
