import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { environment } from './environments/environment';
import { RetailerModule } from './aritnt/retailer/retailer.module';
import { CollectionModule } from './aritnt/collection/collection.module';
import { DistributionModule } from './aritnt/distribution/distribution.module';
import { AdministratorModule } from './aritnt/administrator/administrator.module';
import { DemoModule } from './aritnt/demo/demo.module';
import { AccountModule } from './account/account.module';
import { PartnerModule } from './aritrace/partner/partner.module';
import { ProductionModule } from './aritnt/production/production.module';
import { FulfillmentModule } from './aritnt/fulfillment/fulfillment.module';
import { CMSModule } from './aritnt/cms/cms.module';


if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(CollectionModule)
  .catch(err => console.log(err));
